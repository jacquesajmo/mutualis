import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget
{
  //RegisterPage({ Key key }) : super(key: key);
  final Function toggleView;
  RegisterPage(this.toggleView);

  @override
  _RegisterPageState createState() => _RegisterPageState();

}

class _RegisterPageState extends State<RegisterPage>
{
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _isSecret = true;
  String _password = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 1.75,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment(-1.0, 0.0),
                        end: Alignment(1.0, 0.0),
                        colors: [
                          const Color(0xFF6aa6f8),
                          const Color(0xFF1a60be)
                        ],
                      ),
                    ),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children:[
                          /**/

                          Container(
                            transform:
                            Matrix4.translationValues(0.0, 50.0, 0.0),
                            margin: EdgeInsets.symmetric(horizontal: 20.0),
                            width: double.infinity,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25),
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25),
                              ),
                              color: Color(0xFFFFFFFF),
                              boxShadow: [
                                new BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 50.0,
                                  offset: Offset(0, 0),
                                ),
                              ],
                            ),

                            child: Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(
                                    top: 20.0,
                                    left: 20.0,
                                    right: 20.0,
                                  ),
                                  child: Column(
                                    children: [
                                      Text("password".toUpperCase(), style: TextStyle(fontSize: 30.0),),
                                      SizedBox(height: 60.0),
                                      Form(
                                        key: _formKey,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            SizedBox(height: 10.0),
                                            TextFormField(
                                              keyboardType: TextInputType.visiblePassword,

                                              //controller: emailTextEditingController,
                                              textCapitalization: TextCapitalization.none,
                                              decoration: InputDecoration(
                                                hintText: 'Nom et prénoms',
                                                hintStyle: TextStyle(
                                                  color: Color(0xFFb1b2c4),
                                                ),
                                                border: new OutlineInputBorder(
                                                  //borderSide: BorderSide.none,
                                                  borderRadius: BorderRadius.circular(60),
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context).primaryColor),
                                                  borderRadius: BorderRadius.circular(60),
                                                ),
                                                //fillColor: Colors.black.withOpacity(0.05),
                                                contentPadding: EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 25.0,
                                                ),
                                                prefixIcon: Icon(
                                                  Icons.account_box,
                                                  color: Color(0xFF6aa6f8),
                                                ),
                                                //
                                              ),
                                            ),
                                            SizedBox(height: 10.0),
                                            TextFormField(
                                              decoration: InputDecoration(
                                                hintText: 'Numéro de téléphone',
                                                hintStyle: TextStyle(
                                                  color: Color(0xFFb1b2c4),
                                                ),
                                                border: new OutlineInputBorder(
                                                  //borderSide: BorderSide.none,
                                                  borderRadius: BorderRadius.circular(60),
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                                  borderRadius: BorderRadius.circular(60),
                                                ),
                                                //fillColor: Colors.black.withOpacity(0.05),
                                                contentPadding: EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 25.0,
                                                ),
                                                prefixIcon: Icon(
                                                  Icons.phone_android,
                                                  color: Color(0xFF6aa6f8),
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 10.0),
                                            TextFormField(
                                              obscureText: _isSecret,
                                              onChanged: (value) => setState(() => _password = value ),
                                              validator: (value) => value.isEmpty || value.length < 6 ? "Please enter valid password": null,
                                              decoration: InputDecoration(
                                                prefixIcon: Icon(
                                                  Icons.lock_open,
                                                  color: Color(0xFF6aa6f8),
                                                ),
                                                suffixIcon: InkWell(
                                                    onTap: () => setState(() => _isSecret = ! _isSecret),
                                                    child: Icon(! _isSecret? Icons.visibility : Icons.visibility_off)
                                                ),
                                                hintText: 'Mot de passe',
                                                hintStyle: TextStyle(
                                                  color: Color(0xFFb1b2c4),
                                                ),
                                                border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(60),
                                                    borderSide: BorderSide(
                                                        color: Colors.grey
                                                    )
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(60),
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context).primaryColor
                                                    )
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 10.0),
                                            TextFormField(
                                              obscureText: _isSecret,
                                              onChanged: (value) => setState(() => _password = value ),
                                              validator: (value) => value.isEmpty || value.length < 6 ? "Please enter valid password": null,
                                              decoration: InputDecoration(
                                                suffixIcon: InkWell(
                                                    onTap: () => setState(() => _isSecret = ! _isSecret),
                                                    child: Icon(! _isSecret? Icons.visibility : Icons.visibility_off)
                                                ),
                                                prefixIcon: Icon(
                                                  Icons.lock,
                                                  color: Color(0xFF6aa6f8),
                                                ),
                                                hintText: 'Confirmer le mot de passe',
                                                border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(60),
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context).primaryColor
                                                    )
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(60),
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context).primaryColor
                                                    )
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 10.0),

                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ]
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                      top: 80.0,
                      left: 20.0,
                      right: 20.0,
                    ),
                    child: RaisedButton(
                      color: Color(0xFF4894e9),
                      padding: EdgeInsets.all(15),
                      onPressed: () {
                        //signUpAccount();
                      },
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Créer le compte',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          )
      ),
    );
  }

}