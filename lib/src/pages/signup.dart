import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_healthcare_app/src/config/ApiConnexion.dart';
import 'package:flutter_healthcare_app/src/widgets/General.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_page.dart';
/*import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_medical/routes/home.dart';
import 'package:flutter_medical/routes/signIn.dart';
import 'package:flutter_medical/services/authenticate.dart';
import 'package:flutter_medical/services/authentication.dart';
import 'package:flutter_medical/services/database.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_medical/widgets.dart';*/

class SignUpPage extends StatefulWidget {
  final Function toggleView;
  SignUpPage(this.toggleView);
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool isLoading = false;

  final formKey = GlobalKey<FormState>();
  TextEditingController userNameTextEditingController = new TextEditingController();
  TextEditingController userNameLowercaseTextEditingController = new TextEditingController();
  TextEditingController emailTextEditingController = new TextEditingController();
  TextEditingController passwordTextEditingController = new TextEditingController();
  TextEditingController phoneTextEditingController = new TextEditingController();
  TextEditingController cpasswordTextEditingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          child: SingleChildScrollView(
            child: Container(
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height / 1.5,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment(-1.0, 0.0),
                          end: Alignment(1.0, 0.0),
                          colors: [
                            const Color(0xFF6aa6f8),
                            const Color(0xFF1a60be)
                          ],
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              transform:
                              Matrix4.translationValues(0.0, 100.0, 0.0),
                              margin: EdgeInsets.symmetric(horizontal: 20.0),
                              width: double.infinity,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Inscription',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 40,
                                      color: Color(0xFFFFFFFF),
                                    ),
                                  ),
                                  SizedBox(height: 20,),
                                  Text(
                                    'Bienvenue sur Mutualis App',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Color(0xFFFFFFFF),
                                    ),
                                  ),
                                  SizedBox(height: 20,),
                                ],
                              )
                          ),

                          Container(
                            transform:
                            Matrix4.translationValues(0.0, 100.0, 0.0),
                            margin: EdgeInsets.symmetric(horizontal: 20.0),
                            width: double.infinity,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25),
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25),
                              ),
                              color: Color(0xFFFFFFFF),
                              boxShadow: [
                                new BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 50.0,
                                  offset: Offset(0, 0),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(
                                    top: 20.0,
                                    left: 20.0,
                                    right: 20.0,),
                                  child: TextFormField (
                                    keyboardType: TextInputType.visiblePassword,
                                    validator: (val) {
                                      return val.length > 0
                                          ? null
                                          : "Nom et prenoms invalide";
                                    },
                                    controller: userNameTextEditingController,
                                    textCapitalization: TextCapitalization.none,
                                    decoration: InputDecoration(
                                      hintText: 'Nom et Prénoms',
                                      hintStyle: TextStyle(
                                        color: Color(0xFFb1b2c4),
                                      ),
                                      border: new OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color:
                                            Theme.of(context).primaryColor),
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      filled: true,
                                      fillColor: Colors.black.withOpacity(0.05),
                                      contentPadding: EdgeInsets.symmetric(
                                        vertical: 20.0,
                                        horizontal: 25.0,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.account_box,
                                        color: Color(0xFF6aa6f8),
                                      ),
                                      //
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(
                                    top: 20.0,
                                    left: 20.0,
                                    right: 20.0,),
                                  child: TextFormField(
                                    keyboardType: TextInputType.visiblePassword,
                                    validator: (val) {
                                      return val.length > 0
                                          ? null
                                          : "Veuillez entrer votre numéro de téléphone";
                                    },
                                    controller: phoneTextEditingController,
                                    textCapitalization: TextCapitalization.none,
                                    decoration: InputDecoration(
                                      hintText: 'Numéro de téléphone',
                                      hintStyle: TextStyle(
                                        color: Color(0xFFb1b2c4),
                                      ),
                                      border: new OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color:
                                            Theme.of(context).primaryColor),
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      filled: true,
                                      fillColor: Colors.black.withOpacity(0.05),
                                      contentPadding: EdgeInsets.symmetric(
                                        vertical: 20.0,
                                        horizontal: 25.0,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.phone_android,
                                        color: Color(0xFF6aa6f8),
                                      ),
                                      //
                                    ),
                                  ),
                                ),

                                Container(
                                  margin: const EdgeInsets.all(20.0),
                                  child: TextFormField(
                                    keyboardType: TextInputType.visiblePassword,
                                    validator: (val) {
                                      return val.length > 6
                                          ? null
                                          : "Veuillez entrer un mot de passe de plus de 6 caractères";
                                    },
                                    controller: passwordTextEditingController,
                                    obscureText: true,
                                    textCapitalization: TextCapitalization.none,
                                    decoration: InputDecoration(
                                      hintText: 'Mot de passe',
                                      hintStyle: TextStyle(
                                        color: Color(0xFFb1b2c4),
                                      ),
                                      border: new OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color:
                                            Theme.of(context).primaryColor),
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      filled: true,
                                      fillColor: Colors.black.withOpacity(0.05),
                                      contentPadding: EdgeInsets.symmetric(
                                        vertical: 20.0,
                                        horizontal: 25.0,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.lock,
                                        color: Color(0xFF6aa6f8),
                                      ),
                                      //
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(
                                    left: 20.0,
                                    right: 20.0,
                                    bottom: 20.0,
                                  ),
                                  child: TextFormField(
                                    keyboardType: TextInputType.visiblePassword,
                                    validator: (val) {
                                      return val == passwordTextEditingController.text
                                          ? null
                                          : "Les mots de passe ne sont pas identiques";
                                    },
                                    controller: cpasswordTextEditingController,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      hintText: 'Confirmer le mot de passe',
                                      hintStyle: TextStyle(
                                        color: Color(0xFFb1b2c4),
                                      ),
                                      border: new OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color:
                                            Theme.of(context).primaryColor),
                                        borderRadius: BorderRadius.circular(60),
                                      ),
                                      filled: true,
                                      fillColor: Colors.black.withOpacity(0.05),
                                      contentPadding: EdgeInsets.symmetric(
                                        vertical: 20.0,
                                        horizontal: 25.0,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.lock_outline,
                                        color: Color(0xFF6aa6f8),
                                      ),
                                      //
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        top: 100.0,
                        left: 20.0,
                        right: 20.0,
                      ),

                      child:
                      isLoading ? General().mutualisLoader()
                          : RaisedButton(
                        color: Color(0xFF4894e9),
                        padding: EdgeInsets.all(15),
                        onPressed: () async {
                          //signUpAccount();
                          if(formKey.currentState.validate())
                          {
                            setState(() {
                              isLoading = true;
                            });
                            Dio dio = Dio(ApiConnexion().baseOptions());

                            String name = userNameTextEditingController.text;
                            String phone = phoneTextEditingController.text;
                            String password = cpasswordTextEditingController.text;
                            var response = await dio.post('/register', data: {'name': name, 'phone': phone, 'password': password});
                            //widget.onChangedStep(1)
                            if(response != null) {
                              setState(()
                              {
                                isLoading = false;
                              });

                              if(response.data["success"] == true)
                              {
                                SharedPreferences localStorage = await SharedPreferences.getInstance();
                                await localStorage.setString('user', jsonEncode(response.data['user']));
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HomePage()));
                              }else{
                                print(response);
                                AlertDialog(
                                  title: const Text('AlertDialog Tilte'),
                                  content: const Text('AlertDialog description'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () => Navigator.pop(context, 'Cancel'),
                                      child: const Text('Cancel'),
                                    ),
                                    TextButton(
                                      onPressed: () => Navigator.pop(context, 'OK'),
                                      child: const Text('OK'),
                                    ),
                                  ],
                                );
                              }
                            }
                          }
                        },
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Créer le compte',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        top: 20.0,
                        left: 20.0,
                        right: 20.0,
                      ),
                      child: InkWell(
                        child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: Text(
                            'Vous avez déjà un compte? Connectez-vous.',
                            style: TextStyle(
                                color: Color(0xFF4894e9),
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        onTap: () {
                          widget.toggleView();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
