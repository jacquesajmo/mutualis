import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_healthcare_app/src/config/authenticate.dart';
import 'package:flutter_healthcare_app/src/pages/home_page.dart';
import 'package:flutter_healthcare_app/src/theme/light_color.dart';
import 'package:flutter_healthcare_app/src/theme/text_styles.dart';
import 'package:flutter_healthcare_app/src/theme/extention.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  bool isLoggedIn = false;
  void initState() {
    _loadUser();
     Future.delayed(Duration(seconds: 3)).then((_) {
       if(isLoggedIn)
         Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => HomePage()));
       else
         Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Authenticate()));
    });
    super.initState();
  }

  _loadUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = await jsonDecode(localStorage.getString('user'));
    if (user != null) {
      isLoggedIn = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/doctor_face.jpg"),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Positioned.fill(
            child: Opacity(
              opacity: .6,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [LightColor.purpleExtraLight, LightColor.purple],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      tileMode: TileMode.mirror,
                      stops: [.5, 6]),
                ),
              ),
            ),
          ),
         Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: SizedBox(),
                ),
                Image.asset("assets/heartbeat.png", color: Colors.white,height: 100,),
                Text(
                  "Mutualis App",
                  style: TextStyles.h1Style.white,
                ),
                /*Text(
                  "By healthcare Evolution",
                  style: TextStyles.bodySm.white.bold,
                ),*/
                Expanded(
                  flex: 7,
                  child: SizedBox(),
                ),
              ],
            ).alignTopCenter,
        ],
      ),
    );
  }
}
