
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_healthcare_app/src/widgets/AppDrawer.dart';

class AboutPage extends StatefulWidget
{
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<AboutPage>
{

  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget aboutBody()
  {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 100.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('images/logo.jpeg', width: 250,),
          SizedBox(height: 10,),
          Text("MutualisApp v1.0", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
          SizedBox(height: 10,),
          Text("Copyright @2021 Ajmo, Fathi", style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal),),
          SizedBox(height: 12,),
          Text("Mutualis App",
              textAlign: TextAlign.center,
              style: TextStyle(height: 1.8)
            //GoogleFonts.roboto(color: Colors.grey),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("A Propos"),
        //backgroundColor: Theme.of(context).backgroundColor,
        leading: Builder(builder: (context) => // Ensure Scaffold is in context
        GestureDetector(
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
            size: 22.0,
          ),
          onTap: () {
            Navigator.pushNamed(context, '/HomePage');
          },
        ),
          /*IconButton(
            icon: Icon(Icons.menu, size: 30,),
            color: Colors.black,
            onPressed: () => Scaffold.of(context).openDrawer()),*/
        ),
      ),
      key: _drawerKey,
      endDrawer: new AppDrawer(),
      drawerEnableOpenDragGesture: true,
      drawerScrimColor: Colors.black.withOpacity(0.4),
      body: aboutBody(),
    );

  }

}