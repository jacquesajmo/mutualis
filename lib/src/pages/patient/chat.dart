import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_healthcare_app/src/pages/home_page.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final _formKey = GlobalKey<FormState>();
  var _loading = false;
  bool _validate = true;
  TextEditingController _message = new TextEditingController();

  /*_getMyMessages() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = await jsonDecode(localStorage.getString('user'));
    var user_id = user['id'].toString();
    var response = await ApiConnection().dioGetRequest(
        'messages-list/' + user_id,
        checkToken: true,
        hasHeaders: true);

    return response.data;
  }*/

  _callNumber(String phoneNumber) async {
    //launch('tel://+22969343333');
  }

  @override
  void dispose() {
    _message.dispose();
    super.dispose();
  }

  Widget MessagesFound() {
    /*return FutureBuilder(
        future: _getMyMessages(),
        builder: (context, snap) {
          if ((snap.connectionState == ConnectionState.none &&
              snap.hasData == null) ||
              (snap.hasData == false) ||
              (snap.connectionState == ConnectionState.waiting)) {
            return Center(child: General().nonviLoader());
          }

          if (snap.connectionState == ConnectionState.done &&
              snap.data['data'].length == 0) {
            return Center(
              child: Text(
                'Aucun message trouvé',
                style: GoogleFonts.roboto(color: Colors.grey),
              ),
            );
          }

          List<Widget> messages = List();
          snap.data['data'].forEach((item) {
            messages.add(Message(
              item['type'],
              item['content'],
              item['created_at'],
            ));
          });
          return ListView(
            children: messages,
          );
        });*/
  }

  Widget Message(
      String messageType, String messageContent, String messageDate) {
    return Container(
      padding: EdgeInsets.only(left: 14, right: 14, top: 10, bottom: 10),
      child: Column(children: [
        Align(
          alignment: messageType == "receiver"
              ? Alignment.topLeft
              : Alignment.topRight,
          child: Text(
            messageDate,
            style: TextStyle(fontSize: 12),
          ),
        ),
        Align(
          alignment: (messageType == "receiver"
              ? Alignment.topLeft
              : Alignment.topRight),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: (messageType == "receiver"
                  ? Colors.grey.shade200
                  : Theme.of(context).primaryColor),

              /*image: DecorationImage(
                    image: AssetImage(
                        'images/message_shape.png'),
                    fit: BoxFit.fill,
                  ),*/
            ),
            padding: EdgeInsets.all(16),
            child: Text(
              messageContent,
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text("Chat avec le medecin"),
          //backgroundColor: Theme.of(context).backgroundColor,
          leading: Builder(builder: (context) => // Ensure Scaffold is in context
          GestureDetector(
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
              size: 22.0,
            ),
            onTap: () {
              Navigator.pushNamed(context, '/HomePage');
              //Navigator.pop(context, "OK");
              /*Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomePage()) );*/
            },
          ),
            /*IconButton(
            icon: Icon(Icons.menu, size: 30,),
            color: Colors.black,
            onPressed: () => Scaffold.of(context).openDrawer()),*/
          ),
        ),
        body: Stack(
          children: <Widget>[
            //MessagesFound(),
            Message("sender", "Bonjour Docteur", "05/06/2021 10:00"),

            Message("receiver", "Bonjour cher patient", "05/06/2021 10:10"),
            SizedBox(height: 30,),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
                height: 60,
                width: double.infinity,
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      child: TextField(
                        autocorrect: true,
                        controller: _message,
                        decoration: InputDecoration(
                          hintText: "Ecrire un message...",
                          hintStyle: TextStyle(color: Colors.black54),
                          border: InputBorder.none,
                          errorText:
                          !_validate ? 'Votre message est vide' : null,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    FloatingActionButton(
                      onPressed: () async {
                        setState(() {
                          _message.text.isEmpty
                              ? _validate = false
                              : _validate = true;
                        });
                        if (_validate) {
                          SharedPreferences localStorage =
                          await SharedPreferences.getInstance();

                          //var user = await jsonDecode(localStorage.getString('user'));

                          /*var response = await ApiConnection().dioPostRequest(
                              'send-message',
                              {
                                'userId': user['id'],
                                'type': "sender",
                                'content': _message.value.text,
                              },
                              checkToken: true,
                              hasHeaders: true,
                              alice: alice);*/

                          //successDialog(context, response.data['message']);
                          //Alert().simple(context, "Message", response.data['message'], "OK", "");

                          setState(() {
                            _loading = false;
                            _message.clear();
                          });
                        }
                      },
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                        size: 18,
                      ),
                      backgroundColor: Theme.of(context).primaryColor,
                      elevation: 0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
