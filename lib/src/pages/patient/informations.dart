import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_healthcare_app/src/config/ApiConnexion.dart';
import 'package:flutter_healthcare_app/src/theme/light_color.dart';
import 'package:flutter_healthcare_app/src/theme/text_styles.dart';
import 'package:flutter_healthcare_app/src/theme/theme.dart';
import 'package:flutter_healthcare_app/src/widgets/General.dart';
import 'package:flutter_healthcare_app/src/theme/extention.dart';


class InformationsPage extends StatefulWidget {
  @override
  _InformationsState createState() => _InformationsState();
}

class _InformationsState extends State<InformationsPage> {
  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  //Alice alice = Alice();

  /*_registerOnFirebase() {
    _firebaseMessaging.subscribeToTopic('all');
    _firebaseMessaging.getToken().then((token) => print(token));
  }*/

  @override
  void initState() {
    super.initState();

    /*_registerOnFirebase();
    checkNotifications();
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));*/
  }

  /*checkNotifications() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final notification = message['notification'];
        final data = message['data'];

        setState(() {

        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        final notification = message['notification'];
        final data = message['data'];
        setState(() {

        });
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        final notification = message['notification'];
        final data = message['data'];
        setState(() {

        });

      },
    );
  }*/

  _getMyMessages() async {

    Dio dio = Dio(ApiConnexion().baseOptions());
    var response = await dio.get('/actualities');
    //print(response);
    return response.data;
  }

  Widget NotificationsFound() {
    return FutureBuilder(
        future: _getMyMessages(),
        builder: (context, snap) {
          if ((snap.connectionState == ConnectionState.none &&
              snap.hasData == null) ||
              (snap.hasData == false) ||
              (snap.connectionState == ConnectionState.waiting)) {
            return Center(child: General().mutualisLoader());
          }

          if (snap.connectionState == ConnectionState.done &&
              snap.data['data'].length == 0) {
            return Center(
              child: Text(
                'Aucune notification trouvée',
                //style: GoogleFonts.roboto(color: Colors.grey),
              ),
            );
          }

          List<Widget> notifs = List();
          snap.data['data'].forEach((item) {
            notifs.add(WidgetNotification(
                item['title'], item['body'], item['created_at']));
          });
          return ListView(
            children: notifs,
          );
        });
  }

  Widget WidgetNotification(String title, String content, String date) {
    /*return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Card(
        margin: EdgeInsets.only(bottom: 5.0),
        child: Container(
            padding:
            const EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                *//*Container(
                  padding:
                  EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  color: (status.toLowerCase() == "en attente") ? Helpers().APP_PRIMARY_COLOR : Colors.green,
                  child: new Text(status,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold)),
                ),*//*
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Text(title)
                          //new Text(startStation)
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          //isNull(content) ? Text('') : Text(content),
                          //Text(date)
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(''),
                          //isNull(date) ? Text('') : Text(date)
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );*/
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            offset: Offset(4, 4),
            blurRadius: 10,
            color: LightColor.grey.withOpacity(.2),
          ),
          BoxShadow(
            offset: Offset(-3, 0),
            blurRadius: 15,
            color: LightColor.grey.withOpacity(.1),
          )
        ],
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 8),
        child: Column(
          children: [
            ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(13)),
                child: Container(
                  height: 55,
                  width: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: AppTheme.randomColor(context),
                  ),
                  child: Image.asset(
                    "assets/doctor.png",
                    height: 50,
                    width: 50,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              title: Text(title, style: TextStyles.title),
              subtitle: Text(content, style: TextStyles.bodySm,),

              trailing: Icon(
                Icons.keyboard_arrow_right,
                size: 30,
                color: Theme.of(context).primaryColor,
              ),
            ),
            Text(date),
          ],
        )


      ).ripple(() {
        //Navigator.pushNamed(context, "/DetailPage", arguments: model);
      }, borderRadius: BorderRadius.all(Radius.circular(20))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text("Actualites"),
          //backgroundColor: Theme.of(context).backgroundColor,
          leading: Builder(builder: (context) => // Ensure Scaffold is in context
          GestureDetector(
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
              size: 22.0,
            ),
            onTap: () {
              Navigator.pushNamed(context, '/HomePage');
            },
          ),
            /*IconButton(
            icon: Icon(Icons.menu, size: 30,),
            color: Colors.black,
            onPressed: () => Scaffold.of(context).openDrawer()),*/
          ),
        ),
        body: NotificationsFound());
  }
}
