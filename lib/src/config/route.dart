import 'package:flutter/material.dart';
import 'package:flutter_healthcare_app/src/config/authenticate.dart';
import 'package:flutter_healthcare_app/src/pages/about.dart';
import 'package:flutter_healthcare_app/src/pages/detail_page.dart';
import 'package:flutter_healthcare_app/src/pages/doctors_list.dart';
import 'package:flutter_healthcare_app/src/pages/home_page.dart';
import 'package:flutter_healthcare_app/src/pages/login.dart';
import 'package:flutter_healthcare_app/src/pages/patient/appointment.dart';
import 'package:flutter_healthcare_app/src/pages/patient/chat.dart';
import 'package:flutter_healthcare_app/src/pages/patient/informations.dart';
import 'package:flutter_healthcare_app/src/pages/splash_page.dart';
import 'package:flutter_healthcare_app/src/widgets/coustom_route.dart';

class Routes {
  static Map<String, WidgetBuilder> getRoute() {
    return <String, WidgetBuilder>{
      '/': (_) => SplashPage(),
      '/HomePage': (_) => HomePage(),
      '/login': (_) => Authenticate(),
      '/doctors_list' : (_) => DoctorsListPage(),
      '/actualities' : (_) => InformationsPage(),
      '/about' : (_) => AboutPage(),
      '/chat' : (_) => ChatPage(),
      '/appointment' : (_) => AppointmentPage(),
    };
  }

  static Route onGenerateRoute(RouteSettings settings) {
    final List<String> pathElements = settings.name.split('/');
    if (pathElements[0] != '' || pathElements.length == 1) {
      return null;
    }
    switch (pathElements[1]) {
      case "DetailPage":
        return CustomRoute<bool>(
            builder: (BuildContext context) => DetailPage(model: settings.arguments,));

    }
  }
}
