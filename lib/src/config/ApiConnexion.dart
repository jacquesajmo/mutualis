
import 'package:dio/dio.dart';

class ApiConnexion
{
  BaseOptions baseOptions()
  {
    return BaseOptions(
      baseUrl: 'https://4127a1d62c85.ngrok.io/api',
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
  }
}