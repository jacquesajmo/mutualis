import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_healthcare_app/src/pages/login.dart';
import 'package:flutter_healthcare_app/src/pages/signup.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool showSignIn = true;

  @override
  void initState() {
    super.initState();
  }

  void toggleView() {
    setState(() {
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return SignInPage(toggleView);
    } else {
      return SignUpPage(toggleView);
    }
  }
}
