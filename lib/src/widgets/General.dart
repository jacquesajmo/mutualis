import 'package:flutter_healthcare_app/src/pages/profile_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_healthcare_app/src/theme/extention.dart';
import 'package:flutter_healthcare_app/src/theme/light_color.dart';

class General {
  Widget mutualisLoader() {
    return CircularProgressIndicator(
      strokeWidth: 4.0,
      valueColor: new AlwaysStoppedAnimation(
          Color(0xFF6aa6f8)
      ),
    );
  }

  Widget defaultAppBar(BuildContext context) {
    return AppBar(
      title: Text("Mutualis App"),
      elevation: 0,
      backgroundColor: Theme.of(context).backgroundColor,
      leading: Builder(builder: (context) => // Ensure Scaffold is in context
      IconButton(
          icon: Icon(Icons.menu, size: 30,),
          color: Colors.black,
          onPressed: () => Scaffold.of(context).openDrawer()),
      ),
      /*leading: Icon(
        Icons.short_text,
        size: 30,
        color: Colors.black,

      ),*/
      actions: <Widget>[
        Icon(
          Icons.notifications_none,
          size: 30,
          color: LightColor.grey,
        ),
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(13)),
          child: Container(
            // height: 40,
            // width: 40,
            decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
            ),
            child: Image.asset("assets/user.png", fit: BoxFit.fill),
          ),
        ).p(8).ripple((){
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => ProfilePage(),
            ),
          );
        }),
      ],
    );
  }
}